package com.example.roundrobin2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class Historial extends AppCompatActivity {
    TextView edtCantidad,r1,r2,r3,r4,r5,p1,p2,p3,p4,p5,t1,t2,t3,t4,t5,edtTiempo;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFB00020")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        edtCantidad=(TextView) findViewById(R.id.edtCantidad);
        edtTiempo=(TextView) findViewById(R.id.edtTiempo);
        r1=(TextView) findViewById(R.id.r1);
        r2=(TextView) findViewById(R.id.r2);
        r3=(TextView) findViewById(R.id.r3);
        r4=(TextView) findViewById(R.id.r4);
        r5=(TextView) findViewById(R.id.r5);
        p1=(TextView) findViewById(R.id.p1);
        p2=(TextView) findViewById(R.id.p2);
        p3=(TextView) findViewById(R.id.p3);
        p4=(TextView) findViewById(R.id.p4);
        p5=(TextView) findViewById(R.id.p5);
        t1=(TextView) findViewById(R.id.t1);
        t2=(TextView) findViewById(R.id.t2);
        t3=(TextView) findViewById(R.id.t3);
        t4=(TextView) findViewById(R.id.t4);
        t5=(TextView) findViewById(R.id.t5);
        TextView[] raf = new TextView[]{ r1, r2, r3, r4, r5 };
        TextView[] pro = new TextView[]{ p1, p2, p3, p4, p5 };
        TextView[] tie = new TextView[]{ t1, t2, t3, t4, t5 };
        Bundle extras = getIntent().getExtras();
        int[][] matrizB = (int[][]) extras.get("arreglo");
        int count=0;

        for (int i = 0; i <matrizB.length; i++)
        {
            if (matrizB[i][0]!=0){
                count++;
                if (count==1){
                    edtCantidad.setText(String.valueOf(count)+" proceso");
                }else{
                    edtCantidad.setText(String.valueOf(count)+" procesos");
                }

                if (matrizB[i][2]==1){
                    edtTiempo.setText(String.valueOf(matrizB[i][2])+" segundo");
                    tie[i].setText(String.valueOf(matrizB[i][2])+" segundo");
                }else{
                    edtTiempo.setText(String.valueOf(matrizB[i][2])+" segundos");
                    tie[i].setText(String.valueOf(matrizB[i][2])+" segundos");
                }

                pro[i].setText(String.valueOf(matrizB[i][0]));
                raf[i].setText(String.valueOf(matrizB[i][1]));

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_reboot:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                return true;
            case R.id.action_settings:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}