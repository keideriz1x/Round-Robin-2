package com.example.roundrobin2;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuInflater;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.Queue;

public class MainActivity extends AppCompatActivity {
    Queue<Proceso> cola= new LinkedList();
    Queue<Proceso> historial= new LinkedList();
    Button btnAgregar,btnHistorial,btnIniciar;
    EditText edtRafaga,edtQuantum;
    TextView r1,r2,r3,r4,r5,p1,p2,p3,p4,p5,res1,res2,res3,res4,res5,e1,e2,e3,e4,e5,edtProceso,edtPorcentaje;
    private int cont=0;
    int tempo=0;
    int tiempo_total=1;
    int [][] response= new int [5][3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        btnAgregar=(Button)findViewById(R.id.btnAgregar);
        btnIniciar=(Button)findViewById(R.id.btnIniciar);
        btnHistorial=(Button)findViewById(R.id.btnHistorial);
        edtRafaga = (EditText) findViewById(R.id.edtRafaga);
        edtQuantum = (EditText) findViewById(R.id.edtQuantum);
        edtProceso=(TextView) findViewById(R.id.edtProceso);
        edtPorcentaje=(TextView) findViewById(R.id.edtPorcentaje);
        r1=(TextView) findViewById(R.id.r1);
        r2=(TextView) findViewById(R.id.r2);
        r3=(TextView) findViewById(R.id.r3);
        r4=(TextView) findViewById(R.id.r4);
        r5=(TextView) findViewById(R.id.r5);
        p1=(TextView) findViewById(R.id.p1);
        p2=(TextView) findViewById(R.id.p2);
        p3=(TextView) findViewById(R.id.p3);
        p4=(TextView) findViewById(R.id.p4);
        p5=(TextView) findViewById(R.id.p5);
        res1=(TextView) findViewById(R.id.res1);
        res2=(TextView) findViewById(R.id.res2);
        res3=(TextView) findViewById(R.id.res3);
        res4=(TextView) findViewById(R.id.res4);
        res5=(TextView) findViewById(R.id.res5);
        e1=(TextView) findViewById(R.id.e1);
        e2=(TextView) findViewById(R.id.e2);
        e3=(TextView) findViewById(R.id.e3);
        e4=(TextView) findViewById(R.id.e4);
        e5=(TextView) findViewById(R.id.e5);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView[] raf = new TextView[]{ r1, r2, r3, r4, r5 };
                TextView[] pro = new TextView[]{ p1, p2, p3, p4, p5 };
                TextView[] res = new TextView[]{ res1, res2, res3, res4, res5 };
                TextView[] est = new TextView[]{ e1, e2, e3, e4, e5 };
                String rafaga= edtRafaga.getText().toString();
                        if (!rafaga.isEmpty()){
                            int number=Integer.parseInt(rafaga);
                            if (number>0 && number<11){
                                if(cont<5){
                                    int value=Integer.parseInt(String.valueOf(cont+1));
                                    String num=String.valueOf(value);
                                    pro[cont].setText(num);
                                    raf[cont].setText(rafaga);
                                    res[cont].setText(rafaga);
                                    est[cont].setText("Listo");
                                    cola.add(new Proceso( Integer.parseInt(num) ,Integer.parseInt(rafaga)));
                                    cont++;
                                }else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setTitle("¡Cantidad de procesos!");
                                    builder.setMessage("Solo se pueden agregar maximo 5 procesos.");
                                    builder.setPositiveButton("Aceptar", null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }else{
                                edtRafaga.setError("Número fuera de rango.");
                                edtRafaga.requestFocus();
                            }
                        }else{
                            edtRafaga.setError("Introduce un número.");
                            edtRafaga.requestFocus();
                        }
            }
        });

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String q=edtQuantum.getText().toString();
                if (!q.isEmpty()){
                    int qu=Integer.parseInt(q);
                    if (qu>0 && qu<4){
                        new BackgroundTask().execute();
                    }else{
                        edtQuantum.setError("Número fuera de rango.");
                        edtQuantum.requestFocus();
                    }
                }else{
                    edtQuantum.setError("Introduce un número.");
                    edtQuantum.requestFocus();
                }
            }
        });

        btnHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Historial();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Creado por: Julian David Rincon Espinosa y Juan Diego Moreno Valero.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    public class BackgroundTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

        }

        @Override
        protected void onPreExecute() {

        }

        @SuppressLint("SetTextI18n")
        @Override
        protected Void doInBackground(Void... arg0) {

                int quantum=Integer.parseInt(edtQuantum.getText().toString());
                TextView[] raf = new TextView[]{ r1, r2, r3, r4, r5 };
                TextView[] pro = new TextView[]{ p1, p2, p3, p4, p5 };
                TextView[] res = new TextView[]{ res1, res2, res3, res4, res5 };
                TextView[] est = new TextView[]{ e1, e2, e3, e4, e5 };

                TextView contador_proceso= (TextView)findViewById(R.id.edtProceso);

                while (cola.size()!=0)
                {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Proceso actual= cola.poll();
                    for (int i=0;i<cont;i++){
                        if (Integer.parseInt(res[i].getText().toString())!=0){
                            est[i].setText("********");
                        }
                    }
                    if( actual.getResiduo()>=quantum ){
                        actual.setResiduo( actual.getResiduo()- quantum);
                    }else{
                        actual.setResiduo( actual.getResiduo()- actual.getResiduo());
                    }

                    contador_proceso.setText(Integer.toString(actual.getNum_proceso()));
                    res[actual.getNum_proceso()-1].setText(Integer.toString(actual.getResiduo()));
                    est[actual.getNum_proceso()-1].setText("En Proceso");

                    int rafaga= Integer.parseInt(raf[actual.getNum_proceso() - 1].getText().toString());
                    int residuo= Integer.parseInt(res[actual.getNum_proceso() - 1].getText().toString());
                    int Total=(rafaga-residuo)*100/rafaga;
                    edtPorcentaje.setText(Integer.toString(Total)+"%");

                    if(actual.getResiduo()==0)
                    {
                        est[actual.getNum_proceso()-1].setText("Terminado");
                        actual.setTiempo_total(tiempo_total);

                        response[tempo][0]=actual.getNum_proceso();
                        response[tempo][1]=actual.getRafaga();
                        response[tempo][2]=actual.getTiempo_total();

                        tempo+=1;

                    }
                    else{
                        cola.add(actual);

                    }
                    tiempo_total+=1;
                }

            return null;}
    }

    public void Historial() {
        Intent intent = new Intent(this, Historial.class);
        intent.putExtra("arreglo",response);

        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_reboot:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                return true;
            case R.id.action_settings:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}