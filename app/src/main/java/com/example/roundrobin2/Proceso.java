package com.example.roundrobin2;

public class Proceso {

    protected int num_proceso;
    protected int rafaga;
    protected int residuo;
    protected int estado;
    protected  int tiempo_total;

    public Proceso(int num_proceso, int rafaga) {
        this.num_proceso = num_proceso;
        this.rafaga = rafaga;
        this.residuo=rafaga;
    }

    Proceso() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getNum_proceso() {
        return num_proceso;
    }

    public void setNum_proceso(int num_proceso) {
        this.num_proceso = num_proceso;
    }

    public int getRafaga() {
        return rafaga;
    }

    public void setRafaga(int rafaga) {
        this.rafaga = rafaga;
    }

    public int getResiduo() {
        return residuo;
    }

    public void setResiduo(int residuo) {
        this.residuo = residuo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getTiempo_total() {
        return tiempo_total;
    }

    public void setTiempo_total(int tiempo_total) {
        this.tiempo_total = tiempo_total;
    }
}
